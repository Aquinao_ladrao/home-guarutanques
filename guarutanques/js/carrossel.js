$('.carrossel').slick({
    dots: false,
    infinite: true,
    speed: 200,
    slidesToShow: 1,
    adaptiveHeight: true,
    autoplay: true
  });


  
$('.carrossel-acessorios').slick({
  slidesToShow: 3,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 2000,
});